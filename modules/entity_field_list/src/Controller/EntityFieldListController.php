<?php

namespace Drupal\entity_field_list\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class EntityFieldListController extends ControllerBase {

  public function renderPage() {
    $data = \Drupal::service('views.views_data')->getAll();
    return new JsonResponse($data);
    $json = [];
    foreach ($data as $table => $table_data){
      foreach ($table_data as $field => $field_data){

      }
    }




    $entity_manager = \Drupal::entityTypeManager();
    $entity_types = $entity_manager->getDefinitions();

    $entity_field_info = [];

    foreach ($entity_types as $entity_type_id => $entity_type) {
      // Skip entity types without base fields
      if (!is_a($entity_type->getClass(), ContentEntityInterface::class,TRUE)) {
        continue;
      }

      $bundle_info = \Drupal::service('entity_type.bundle.info')->getBundleInfo($entity_type_id);

      foreach ($bundle_info as $bundle => $bundle_data) {
        $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions($entity_type_id, $bundle);

        foreach ($fields as $field_name => $field_definition) {
		  if ($field_definition->getFieldStorageDefinition()->isBaseField()) {
			$entity_field_info[$entity_type_id][$bundle]['fields'][$field_name] = [
			  'label' => $field_definition->getLabel(),
			  'field_type' => $field_definition->getType(),
			  'machine_name' => $field_definition->getName(),
			  'table' => $field_definition->getTargetEntityTypeId() . '__' . $field_definition->getName(),
			];
		  }
		}


      }
    }

    return new JsonResponse($entity_field_info);
  }

}
