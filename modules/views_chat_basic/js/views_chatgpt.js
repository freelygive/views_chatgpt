let autoScroll = true;
let ignoreScrollEvent = false;

function handleChatSubmit(e) {
  e.preventDefault();

  jQuery('#edit-response').val('').css('background-color', '');

  const text = jQuery('#edit-text').val();
  const model = jQuery('#edit-model').val();
  const temperature = jQuery('#edit-temperature').val();
  const max_tokens = jQuery('#edit-max-tokens').val();
  const system = jQuery('#edit-system').val();
  const body = {
    model:model,
    messages:[
      {role:'system',content:system},
      {role:'user',content:text}
    ],
    temperature:temperature,
    max_tokens:max_tokens
  }

  let progress = 0;

  jQuery.ajax({
    url: '/chatgpt/query',
    method:'POST',
    data: JSON.stringify(body),
    xhrFields: {
      onprogress: function(event){
        let response = jQuery("#edit-response").val();
        response += event.currentTarget.response.substring(progress);
        progress = event.loaded;
        jQuery("#edit-response").val(response);

        // Only auto-scroll if enabled
        if (autoScroll) {
          ignoreScrollEvent = true; // We're about to programmatically scroll, ignore this event
          jQuery('#edit-response').scrollTop(jQuery('#edit-response')[0].scrollHeight);
        }
      }
    }
  })
  .done(function (data) {
    jQuery('#edit-response').css('background-color', '#D7FAD1');
  });
}

jQuery(document).ready(function() {
  jQuery("#edit-generate").click(handleChatSubmit);

  // Track scroll on textarea
  jQuery("#edit-response").on('scroll', function() {
    if(ignoreScrollEvent) { // If we've scrolled programmatically, ignore this event
      ignoreScrollEvent = false;
      return;
    }

    // Disable autoScroll if user manually scrolls up
    // Re-enable autoScroll if user scrolls back to the bottom within the tolerance
    autoScroll = (this.scrollTop + this.offsetHeight >= this.scrollHeight);
  });
});
