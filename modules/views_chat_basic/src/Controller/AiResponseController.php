<?php

namespace Drupal\views_chat_basic\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Handles AJAX requests for a ChatGPT prompt.
 */
class AiResponseController extends ControllerBase {

  /**
   * The OpenAI client.
   *
   * @var \OpenAI\Client
   */
  protected $client;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->client = $container->get('openai.client');
    return $instance;
  }

  /**
   * Invokes ChatGPT and streams back a response.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The HTTP Request.
   */
  public function getAiResponse(Request $request) {

    $body = json_decode($request->getContent());

    $http_response = new StreamedResponse();
    $http_response->setCallback(function() use($body) {
      $stream = $this->client->chat()->createStreamed([
        'model' => $body->model,
        'messages' => $body->messages,
        'temperature' => (int) $body->temperature,
        'max_tokens' => (int) $body->max_tokens,
      ]);

      foreach($stream as $response) {
        $text = $response->choices[0]->delta->content;
        echo $text;
        ob_flush();
        flush();
      }
    });
    $http_response->send();
    exit();
  }

}
