<?php

namespace Drupal\views_chat_basic\Form;

use Drupal\Component\Serialization\Exception\InvalidDataTypeException;
use Drupal\config\StorageReplaceDataWrapper;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Config\ConfigImporter;
use Drupal\Core\Config\ConfigImporterException;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\Importer\ConfigImporterBatch;
use Drupal\Core\Config\StorageComparer;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ModuleInstallerInterface;
use Drupal\Core\Extension\ThemeExtensionList;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Installer\InstallerKernel;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\Core\Url;
use OpenAI\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Class ImportChatViewForm.
 */
class ImportChatViewForm extends FormBase {

  /**
   * If the config exists, this is that object. Otherwise, FALSE.
   *
   * @var \Drupal\Core\Config\Config|\Drupal\Core\Config\Entity\ConfigEntityInterface|bool
   */
  protected $configExists = FALSE;

  /**
   * The submitted data needing to be confirmed.
   *
   * @var array
   */
  protected $data = [];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'import_chat_view_form';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('openai.client'),
      $container->get('entity_type.manager'),
      $container->get('config.storage'),
      $container->get('renderer'),
      $container->get('event_dispatcher'),
      $container->get('config.manager'),
      $container->get('lock.persistent'),
      $container->get('config.typed'),
      $container->get('module_handler'),
      $container->get('module_installer'),
      $container->get('theme_handler'),
      $container->get('extension.list.module'),
      $container->get('extension.list.theme')
    );
  }

  /**
   * Constructor for the import form.
   *
   * @param \OpenAI\Client $client
   *  The OpenAI client.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *  Entity type manager.
   * @param \Drupal\Core\Config\StorageInterface $configStorage
   *  Config storage.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *  Renderer.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *  Event dispatcher.
   * @param \Drupal\Core\Config\ConfigManagerInterface $configManager
   *  Config manager.
   * @param \Drupal\Core\Lock\LockBackendInterface $lock
   *  Lock.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *  Typed config.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *  Module handler.
   * @param \Drupal\Core\Extension\ModuleInstallerInterface $moduleInstaller
   *  Module installer.
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $themeHandler
   *  Theme installer.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *  Extension list module.
   * @param \Drupal\Core\Extension\ThemeExtensionList $themeExtensionList
   *  Extension list theme.
   */
  public function __construct(
    protected Client                      $client,
    protected EntityTypeManagerInterface  $entityTypeManager,
    protected StorageInterface            $configStorage,
    protected RendererInterface           $renderer,
    // Services necessary for \Drupal\Core\Config\ConfigImporter.
    protected EventDispatcherInterface    $eventDispatcher,
    protected ConfigManagerInterface      $configManager,
    protected LockBackendInterface        $lock,
    protected TypedConfigManagerInterface $typedConfigManager,
    protected ModuleHandlerInterface      $moduleHandler,
    protected ModuleInstallerInterface    $moduleInstaller,
    protected ThemeHandlerInterface       $themeHandler,
    protected ModuleExtensionList         $moduleExtensionList,
    protected ThemeExtensionList          $themeExtensionList) {
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    if (!empty($this->data)) {
      $form['warning'] = [
        '#type' => 'markup',
        '#markup' => $this->t('<p>Are you sure you want to import View %view?</p>', [
          '%view' => $this->data['response']['label'],
        ]),
      ];
      $form['cancel'] = [
        '#type' => 'submit',
        '#value' => $this->t('Cancel and go back'),
        '#cancel' => TRUE,
      ];

      $form['confirm'] = [
        '#type' => 'submit',
        '#value' => $this->t('Confirm and import'),
        '#attributes' => [
          'class' => ['button--primary'],
        ],
      ];
      return $form;
    }

    $form['#attached']['library'][] = 'views_chat_basic/views_chatgpt';
    $form['text'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Prompt'),
      '#description' => $this->t('Enter your text here. When submitted, OpenAI will generate a response from its Chats endpoint and create a View. Please note that each query counts against your API usage for OpenAI. Based on the complexity of your text, OpenAI traffic, and other factors, a response can sometimes take up to 10-15 seconds to complete. Please allow the operation to finish.  Be cautious not to exceed the requests per minute quota (20/Minute by default), or you may be temporarily blocked.'),
      '#default_value' => $form_state->getValue('text') ?? '',
    ];
    $form['response'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Response'),
      '#prefix' => '<div id="openai-chatgpt-response">',
      '#suffix' => '</div>',
      '#description' => $this->t('The response from OpenAI will appear in the textbox above.'),
      '#default_value' => $form_state->getValue('response') ?? '',
    ];

    $form['options'] = [
      '#type' => 'details',
      '#title' => t('Options'),
      '#description' => t('Set various options related to how ChatGPT generates its response.'),
      '#open' => FALSE,
    ];

    $form['options']['model'] = [
      '#type' => 'select',
      '#title' => $this->t('Model'),
      '#options' => [
        'gpt-4' => 'gpt-4 (currently beta invite only)',
        'gpt-4-32k' => 'gpt-4-32k (currently beta invite only)',
        'gpt-3.5-turbo' => 'gpt-3.5-turbo',
        'gpt-3.5-turbo-26k' => 'gpt-3.5-turbo-16k (currently beta invite only)',
        'gpt-3.5-turbo-0301' => 'gpt-3.5-turbo-0301',
      ],
      '#default_value' => $form_state->getValue('model') ?? 'gpt-4',
      '#description' => $this->t('Select which model to use to analyze text. See the <a href="@link">model overview</a> for details about each model.', ['@link' => 'https://platform.openai.com/docs/models/gpt-3.5']),
    ];

    $form['options']['temperature'] = [
      '#type' => 'number',
      '#title' => $this->t('Temperature'),
      '#min' => 0,
      '#max' => 2,
      '#step' => .1,
      '#default_value' => $form_state->getValue('temperature') ?? '0.4',
      '#description' => $this->t('What sampling temperature to use, between 0 and 2. Higher values like 0.8 will make the output more random, while lower values like 0.2 will make it more focused and deterministic.'),
    ];

    $form['options']['max_tokens'] = [
      '#type' => 'number',
      '#title' => $this->t('Max tokens'),
      '#min' => 0,
      '#max' => 8192,
      '#step' => 1,
      '#default_value' => $form_state->getValue('max_tokens') ?? '4096',
      '#description' => $this->t('The maximum number of tokens to generate in the completion. The token count of your prompt plus max_tokens cannot exceed the model\'s context length. Most models have a context length of 2048 tokens (except for the newest models, which support 4096).'),
    ];

    $form['options']['system'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Profile'),
      '#default_value' => $form_state->getValue('system') ?? 'You are an automated tool for helping a user create Views in Drupal. You will always return your answers as YAML to be imported into Drupal automatically. End all your answers with #EndofFile.',
      '#description' => $this->t('The "profile" helps set the behavior of the ChatGPT response. You can change/influence how it response by adjusting the above instruction. If you want to change this value after starting a conversation, you will need to reload the form first.'),
      '#required' => TRUE,
    ];
    $form['actions'] = [
      '#type' => 'actions',
    ];
    $form['actions']['generate'] = [
      '#type' => 'submit',
      '#value' => $this->t('Generate YAML'),
      '#attributes' => [
        'class' => empty($form_state->getValue('response')) ? ['button--primary'] : [],
      ],
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Create View'),
      '#attributes' => [
        'class' => $form_state->getValue('response') ? ['button--primary'] : [],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // The confirmation step needs no additional validation.
    if ($this->data) {
      return;
    }

    try {
      // Decode the submitted import.
      $data = Yaml::decode($form_state->getValue('response'));
    }
    catch (InvalidDataTypeException $e) {
      $form_state->setErrorByName('import', $this->t('The import failed with the following message: %message', ['%message' => $e->getMessage()]));
    }

    // Validate for config entities.
    $definition = $this->entityTypeManager->getDefinition('view');
    $id_key = $definition->getKey('id');

    $entity_storage = $this->entityTypeManager->getStorage('view');
    // If an entity ID was not specified, set an error.
    if (!isset($data[$id_key])) {
      $form_state->setErrorByName('import', $this->t('Missing ID key "@id_key" for this @entity_type import.', [
        '@id_key' => $id_key,
        '@entity_type' => $definition->getLabel(),
      ]));
      return;
    }

    $config_name = $definition->getConfigPrefix() . '.' . $data[$id_key];
    // If there is an existing entity, ensure matching ID and UUID.
    if ($entity = $entity_storage->load($data[$id_key])) {
      $this->configExists = $entity;
      if (!isset($data['uuid'])) {
        $form_state->setErrorByName('import', $this->t('An entity with this machine name already exists but the import did not specify a UUID.'));
        return;
      }
      if ($data['uuid'] !== $entity->uuid()) {
        $form_state->setErrorByName('import', $this->t('An entity with this machine name already exists but the UUID does not match.'));
        return;
      }
    }
    // If there is no entity with a matching ID, check for a UUID match.
    elseif (isset($data['uuid']) && $entity_storage->loadByProperties(['uuid' => $data['uuid']])) {
      $form_state->setErrorByName('import', $this->t('An entity with this UUID already exists but the machine name does not match.'));
    }

    // Use ConfigImporter validation.
    if (!$form_state->getErrors()) {
      $source_storage = new StorageReplaceDataWrapper($this->configStorage);
      $source_storage->replaceData($config_name, $data);
      $storage_comparer = new StorageComparer($source_storage, $this->configStorage);

      $storage_comparer->createChangelist();
      if (!$storage_comparer->hasChanges()) {
        $form_state->setErrorByName('import', $this->t('There are no changes to import.'));
      }
      else {
        $config_importer = new ConfigImporter(
          $storage_comparer,
          $this->eventDispatcher,
          $this->configManager,
          $this->lock,
          $this->typedConfigManager,
          $this->moduleHandler,
          $this->moduleInstaller,
          $this->themeHandler,
          $this->getStringTranslation(),
          $this->moduleExtensionList,
          $this->themeExtensionList
        );

        try {
          $config_importer->validate();
          $form_state->set('config_importer', $config_importer);
        }
        catch (ConfigImporterException $e) {
          // There are validation errors.
          $item_list = [
            '#theme' => 'item_list',
            '#items' => $config_importer->getErrors(),
            '#title' => $this->t('The configuration cannot be imported because it failed validation for the following reasons:'),
          ];
          $form_state->setErrorByName('import', $this->renderer->render($item_list));
        }
      }
    }

    // Store the decoded version of the submitted import.
    $form_state->setValueForElement($form['response'], $data);
  }

  public function getResponse(array &$form, FormStateInterface $form_state) {
    $storage = $form_state->getStorage();
    $last_response = end($storage['messages']);
    $form['response']['#value'] = trim($last_response['content']) ?? $this->t('No answer was provided.');
    return $form['response'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // If this form has not yet been confirmed, store the values and rebuild.
    if (!$this->data) {
      $form_state->setRebuild();
      $this->data = $form_state->getValues();
      return;
    }

    // If the user has cancelled the import, push data back to form state, clear
    // data and rebuild the form.
    if ($form_state->getTriggeringElement()['#cancel']) {
      // Re-encode the response as a string for display.
      $this->data['response'] = Yaml::encode($this->data['response']);
      $form_state->setValues($this->data);
      $form_state->setRebuild();
      // Clear the data to force re-submission of main form.
      unset ($this->data);
      return;
    }

    /** @var \Drupal\Core\Config\ConfigImporter $config_importer */
    $config_importer = $form_state->get('config_importer');
    if ($config_importer->alreadyImporting()) {
      $this->messenger()
        ->addError($this->t('Another request may be importing configuration already.'));
    }
    else {
      try {
        $sync_steps = $config_importer->initialize();
        $batch_builder = (new BatchBuilder())
          ->setTitle($this->t('Importing configuration'))
          ->setFinishCallback([static::class, 'batchFinish'])
          ->setInitMessage($this->t('Starting configuration import.'))
          ->setProgressMessage($this->t('Completed @current step of @total.'))
          ->setErrorMessage($this->t('Configuration import has encountered an error.'));
        foreach ($sync_steps as $sync_step) {
          $batch_builder->addOperation([
            static::class,
            'batchProcess',
          ], [$config_importer, $sync_step, $this->data]);
        }
        batch_set($batch_builder->toArray());
      }
      catch (ConfigImporterException $e) {
        // There are validation errors.
        $this->messenger()
          ->addError($this->t('The configuration import failed for the following reasons:'));
        foreach ($config_importer->getErrors() as $message) {
          $this->messenger()->addError($message);
        }
      }
    }
  }

  /**
   * Proces each step of the config import.
   *
   * Note that we delegate to the base config importer to do the work. The only
   * reason we wrap this in our own method is so we can take additional
   * parameters which are used on completion.
   *
   * @param \Drupal\Core\Config\ConfigImporter $config_importer
   *   The batch config importer object to persist.
   * @param string $sync_step
   *   The synchronization step to do.
   * @param array $yaml
   *   The raw yaml to import.
   * @param array $context
   *   The batch context.
   */
  public static function batchProcess(ConfigImporter $config_importer, $sync_step, array $yaml, &$context) {
    ConfigImporterBatch::process($config_importer, $sync_step, $context);
    // Store our yaml in the results collection for use in the finish callback.
    $context['results']['yaml'] = $yaml;
  }

  /**
   * Finish batch.
   *
   * This function is a static function to avoid serializing the ConfigSync
   * object unnecessarily.
   *
   * @param bool $success
   *   Indicate that the batch API tasks were all completed successfully.
   * @param array $results
   *   An array of all the results that were updated.
   * @param array $operations
   *   A list of the operations that had not been completed by the batch API.
   */
  public static function batchFinish($success, $results, $operations) {
    $messenger = \Drupal::messenger();
    if ($success) {
      if (!empty($results['errors'])) {
        $logger = \Drupal::logger('config_sync');
        foreach ($results['errors'] as $error) {
          $messenger->addError($error);
          $logger->error($error);
        }
        $messenger->addWarning(t('The configuration was imported with errors.'));
      }
      elseif (!InstallerKernel::installationAttempted()) {
        $view_id = $results['yaml']['response']['id'];
        // Display a success message when not installing Drupal.
        $messenger->addStatus(t('The configuration was imported successfully. <a href=":path">Click here to edit the View</a>', [
          ':path' => Url::fromRoute('entity.view.edit_form', ['view' => $view_id])
            ->toString(),
        ]));
      }
    }
    else {
      // An error occurred.
      // $operations contains the operations that remained unprocessed.
      $error_operation = reset($operations);
      $message = t('An error occurred while processing %error_operation with arguments: @arguments', [
        '%error_operation' => $error_operation[0],
        '@arguments' => print_r($error_operation[1], TRUE),
      ]);
      $messenger->addError($message);
    }
  }

}
