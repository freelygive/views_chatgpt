## enabling streaming in DDEV
Switch server type to apache-fpm

in the folder `.ddev/apache` create file `apache-streaming.conf`

with the settings:

```
<IfModule proxy_fcgi_module>
    <Proxy "fcgi://localhost/" enablereuse=on flushpackets=on max=10>
    </Proxy>
</IfModule>
```

If not using DDEV might need a different setup.